import * as dialogflowFulfillment from "./dialogflow-fulfillment"

// declare namespace dialogflow {
/* WEBHOOK */
export interface FullfillmentRequestQueryResult {
  queryText: string
  parameters: any
  allRequiredParamsPresent: boolean
  fulfillmentText: string
  fulfillmentMessages: IntentMessage
  intent: Intent
  intentDetectionConfidence: number
  diagnosticInfo: any
  languageCode: string
}
export interface FullfillmentRequestOriginalDetectIntentRequest {
  payload: any
}
export interface FullfillmentRequest {
  responseId: string
  queryResult: FullfillmentRequestQueryResult
  originalDetectIntentRequest: FullfillmentRequestOriginalDetectIntentRequest
  session: string
}
export interface WebhookAnswerResult {
  speech: string
  displayText: string
  source: string
}

/* ENTITY */
export class Entity {
  id?: string
  autoExpansionMode?: EntityAutoExpansionMode
  displayName: string
  entities: Synonyms[]
  name?: string
  kind?: EntityKind
}

export type EntityAutoExpansionMode = "AUTO_EXPANSION_MODE_UNSPECIFIED" | ""

export type EntityKind = "KIND_MAP" | ""

export interface Synonyms {
  synonyms: string[]
  value: string
}

/* INTENTS */
export type DetectIntentRequestLanguageCode =
  | "de-DE"
  | "en-US"
  | "fr-FR"
  | "es-ES"
  | "it-IT"
  | "pl-PL"
  | "nl-NL"

export interface DetectIntentRequestQueryInput {
  text: string
  languageCode?: DetectIntentRequestLanguageCode
}
export interface DetectIntentRequestQueryInputText {
  text: DetectIntentRequestQueryInput
}
export interface DetectIntentRequest {
  session: string
  queryInput: DetectIntentRequestQueryInputText
}
export interface UpdateIntentOptions {
  intent: Intent
}
export interface CreateIntentOptions {
  parent: string
  intent: Intent
}
export interface GetIntentOptions {
  intentView: IntentView
  name: string
}
export type IntentView = "INTENT_VIEW_FULL" | ""

export interface DiagnosticInfo {
  fields: string[] // wahrscheinlich ein string array
}

export interface ChatMessage {
  id?: string
  queryText: string
  allRequiredParamsPresent?: false
  diagnosticInfo?: DiagnosticInfo
  fulfillmentMessages?: any[] // wahrscheinlich ein any array (array ist sicher)
  fulfillmentText?: string
  intent?: string | null
  intentDetectionConfidence?: number
  languageCode?: string
  outputContexts?: OutputContext[] // wahrscheinlich ein any array (array ist sicher)
  parameters?: any[] | null // wahrscheinlich ein any array
  speechRecognitionConfidence?: number
  webhookPayload?: any | null // wahrscheinlich any
  webhookSource?: string
}
export interface IntentResult {
  action?: string
  parameters?: any[]
  messages?: any[]
  outputContexts?: OutputContext[]
}
export interface SystemEntityType {
  types:
    | "sys.language:lang-from"
    | "sys.language:lang-to"
    | "sys.address"
    | "sys.email"
    | "sys.date-period"
    | "sys.duration"
    | "sys.street-address"
    | "sys.date"
    | "sys.time-period"
    | "sys.unit-weight"
    | "sys.music-artist"
    | "sys.percentage"
    | "sys.number"
    | "sys.location"
    | "sys.date-time"
    | "sys.unit-volume"
    | "sys.geo-city"
    | "sys.color"
    | "sys.last-name"
    | "sys.language"
    | "sys.flight-number"
    | "sys.given-name"
    | "sys.phone-number"
    | "sys.any"
    | "sys.currency-name"
    | "sys.url"
    | "sys.zip-code"
    | "sys.time"
    | "sys.geo-country"
    | "sys.geo-state-de"
    | "sys.unit-currency"
}
export interface IntentMessageText {
  text: string[]
}
export interface IntentMessage {
  message: string // zb text
  platform: string
  text: IntentMessageText
}
export interface TrainingPhraseParts {
  alias?: string
  entityType?: string
  text: string
  userDefined?: boolean
}
export interface TrainingPhrase {
  name?: string // ID
  parts: TrainingPhraseParts[]
  type?: string // zB EXAMPLE
}
export interface IntentParameter {
  defaultValue?: string
  displayName: string
  entityTypeDisplayName: string
  isList: boolean
  mandatory: boolean
  name: string
  prompts: string[]
  value: string
}
export interface OutputContext {
  lifespanCount: number // default 5
  name: string
  parameters: any
}
export interface FollowupIntentInfo {
  followupIntentName: string
  parentFollowupIntentName: string
}
export interface IntentError {
  message: string
  intent: Intent
}
export interface Intent {
  _dialogflowProjectId?: string
  _dialogflowSynced?: boolean
  _dialogflowSyncing?: boolean
  _id?: string
  _error?: string
  _movedToId?: string
  _nouns?: string[]
  _answerSummaries?: string[]
  _synonyms?: string[]
  displayName?: string
  name?: string
  action?: string
  defaultResponsePlatforms?: any[] // array auf jedenfall, any ?
  events?: any[] // array auf jedenfall, any ?
  followupIntentInfo?: FollowupIntentInfo[] // array auf jedenfall, any ?
  inputContextNames?: string[] // array auf jedenfall, any ?
  isFallback?: boolean
  mlEnabled?: boolean
  outputContexts?: OutputContext[] // array auf jedenfall, any ?
  parameters?: IntentParameter[]
  parentFollowupIntentName?: string
  priority?: number
  resetContexts?: boolean
  rootFollowupIntentName?: string
  trainingPhrases?: TrainingPhrase[]
  messages?: IntentMessage[]
  answerPhrases?: TrainingPhrase[]
  webhookState?: string
  result?: IntentResult
}
export interface ReturnedIntentData {
  code: number
  message: string
  type: string
  error: boolean
  displayName: string
  name: string
}
export interface ReturnedIntent {
  data?: ReturnedIntentData[]
}
export class ContextsClient {
  matchContextFromContextName(contextName: string): OutputContext[]
}
export interface DialogflowConfigCredentials {
  private_key: string
  client_email: string
}
export interface DialogflowConfig {
  credentials: DialogflowConfigCredentials
}
export interface DialogflowAgentsClient {}
export interface DialogflowSessionsClient {}
export class AgentsClient {
  constructor(dialogflowConfig: DialogflowConfig)
}
export class SessionsClient {
  constructor(dialogflowConfig: DialogflowConfig)
}
export class IntentsClient {
  constructor(dialogflowConfig: DialogflowConfig)
  getIntent(opts: GetIntentOptions): Promise<Intent>
  projectAgentPath(projectId: string): string
  createIntent(opts: CreateIntentOptions)
  updateIntent(opts: UpdateIntentOptions)
}
/* }

declare function dialogflow(): any
export = dialogflow */
export as namespace dialogflow
