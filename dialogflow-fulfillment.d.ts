// https://github.com/dialogflow/dialogflow-fulfillment-nodejs

export interface WebhookClientRequestResponse {
  request: any
  response: any
}
export type RichResponse = {}
export type DialogflowConversation = {}
export class WebhookClient {
  constructor(requestResponseObject: WebhookClientRequestResponse)
  agentVersion: number
  intent: string
  action: string
  parameters: Object
  contexts: string
  requestSource: string
  originalRequest: object
  query: string
  locale: string
  session: string
  consoleMessages: Array<RichResponse>
  add(responses)
  addResponse_(response)
  handleRequest(handler): Promise<any>
  setContext(context): WebhookClient
  clearOutgoingContexts(): WebhookClient
  clearContext(context): WebhookClient
  getContext(contextName): Object
  setFollowupEvent(event)
  conv(): DialogflowConversation | null
}
export as namespace dialogflowFulfillment
